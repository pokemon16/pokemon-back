const axios = require("axios").default;
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const XLSX = require('xlsx')
var path = require('path');

/** Funcion par a obtener radom , depende del total de pokemons max= limite total pokemon */
random = async (min,max)=>{
    return Math.floor((Math.random() * (max - min + 1)) + min);
};
/** Obtiene informacion de pokemon  */
getPokemon = async(req, res, next) => {
    try {
        const list = await axios.get("https://pokeapi.co/api/v2/pokemon?limit=1500");
        if(list){
            const index = await random(1,list.data.count);
            const name = list.data.results[index].name;
            if (name) {
                const pokemon = await axios.get('https://pokeapi.co/api/v2/pokemon/'+name+'/');
                return res.status(200).json({ success: true, message: pokemon.data});
            } else {
                return res.status(404).json({ error: true, message: 'No existen pokemon'});
            }
        }else{
            return res.status(404).json({ error: true, message: 'No existen pokemon'});
        }
    } catch (error) {
        res.status(500).json({ error: true,  message: error});
        next(error);
    }
};
/** Obtiene lista de pokemon del mismo tipo */
listType = async(req, res, next) => {
    const types =  req.query.types || 0
    if (types == 0) {
        return res.status(400).json({ error: true, message: 'Falta tipos'});
    }
    let pokemon = [];
    let pokemons = [];
    try {
        //busca los pokemones que pertescan a los tipos del pokemon seleccionado 
        for (type of types) {
            let item = await axios.get('https://pokeapi.co/api/v2/type/'+type+'/');
            if (item.data) {
                item.data.pokemon = item.data.pokemon.map((item)=>{ 
                    item.type_origin = type
                    return item
                });
                pokemon.push(item.data.pokemon)
            }
        }
        pokemon = pokemon.flat();
        pokemon = pokemon.map((item)=>{ return {'name':item.pokemon.name,'type_origin': item.type_origin}});
        if (pokemon) {
            for (namePokemon of pokemon) {
                const pokemonDetalle = await axios.get('https://pokeapi.co/api/v2/pokemon/'+namePokemon.name+'/');
                if (pokemonDetalle.data) {
                    pokemonDetalle.data.type_origin = namePokemon.type_origin
                    pokemons.push(pokemonDetalle.data)
                }
               console.log(pokemons.length)
            }
        }
        pokemons = pokemons.map((item)=>{ 
            return {
                'nombre':item.name,
                'tipo': item.type_origin,
                'ataque': item.stats[1].base_stat,
                'defensa': item.stats[2].base_stat,
            }
        });
        return res.status(200).json({ success: true, message: pokemons});
    } catch (error) {
        res.status(500).json({ error: true,  message: error});
        next(error);
    }
};
/** Genera csv  */
csvType = async(req, res, next) => {
    const url = path.join(__dirname, '../public');
    const pokemons =  req.body.pokemons || 0;
    if (pokemons == 0) {
        return res.status(400).json({ error: true, message: 'Data incompleta'});
    }
    try {
        const csvWriter = createCsvWriter({
            path: 'public/csv/pokemon.csv',
            header: [
              {id: 'nombre', title: 'Pokemon'},
              {id: 'tipo', title: 'Tipo '},
              {id: 'ataque', title: 'Ataque '},
              {id: 'defensa', title: 'Defensa '},
            ]
        });
        csvWriter.writeRecords(pokemons)
        .then(()=> res.status(200).json({ success: true, message: 'CSV creado con éxito ',link:'csv/pokemon.csv'}))
        .catch(()=> res.status(404).json({ error: true,  message: 'Ha ocurrio un error'}))
    } catch (error) {
        res.status(500).json({ error: true,  message: error});
        next(error);
    }
};
/** Genera xslx  */
excelType = async(req, res, next) => {
    const pokemons =  req.body.pokemons || 0;
    if (pokemons == 0) {
        return res.status(400).json({ error: true, message: 'Data incompleta'});
    }
    try {
        let data = XLSX.utils.json_to_sheet(pokemons);
        const workbook = XLSX.utils.book_new();
        const filename = "pokemon";
        const url = 'public/excel/pokemon'
        XLSX.utils.book_append_sheet(workbook, data, filename);
        XLSX.writeFile(workbook, `${url}.xlsx`);
        return res.status(200).json({ success: true, message: 'Excel realizado con éxito ', link:'excel/pokemon.xlsx'});
    } catch (error) {
        res.status(500).json({ error: true,  message: error});
        next(error);
    }
};

module.exports = {
    getPokemon,listType,excelType,csvType
};