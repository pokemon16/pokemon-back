const express = require('express');
const router = express.Router();
const pokemon = require('./pokemon');

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.use('/pokemon', pokemon);

module.exports = router;
