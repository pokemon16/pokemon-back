var express = require('express');
var router = express.Router();
const { pokemonController} = require('../controllers');


router.get('/getPokemon', pokemonController.getPokemon);
router.get('/listType', pokemonController.listType);
router.post('/csvType', pokemonController.csvType);
router.post('/excelType', pokemonController.excelType);
module.exports = router;